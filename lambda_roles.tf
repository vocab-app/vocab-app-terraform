resource "aws_iam_role" "lambda_roles" {
  for_each = toset(var.lambda_roles_list)

  name               = each.value
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "function_sns_publish_attachment" {
  for_each = {
    function_logging_arn = aws_iam_policy.function_logging_policies[var.orchestrator_function].arn,
    sns_publishing_arn   = aws_iam_policy.sns_publishing_policy.arn
  }

  role       = aws_iam_role.lambda_roles["sns_publish"].name
  policy_arn = each.value
}

resource "aws_iam_role_policy_attachment" "only_receive_sqs_messages" {
  for_each = {
    function_logging     = aws_iam_policy.function_logging_policies[var.postToNotionDB_function].arn
    sqs_receive_messages = aws_iam_policy.sqs_receive_messages[var.postToNotionDB_function].arn
  }
  role       = aws_iam_role.lambda_roles["only_sqs_receive"].name
  policy_arn = each.value
}

resource "aws_iam_role_policy_attachment" "function_upload_to_s3_attachment" {
  for_each = {
    function_logging_arn = aws_iam_policy.function_logging_policies[var.create_image_function].arn,
    sqs_receive_messages = aws_iam_policy.sqs_receive_messages[var.create_image_function].arn
    upload_to_s3_arn     = aws_iam_policy.upload_to_s3.arn
  }

  role       = aws_iam_role.lambda_roles["s3_upload"].name
  policy_arn = each.value
}
