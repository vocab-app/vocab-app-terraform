resource "aws_sns_topic" "dict_data_topic" {
  name = "dict-data-topic"
}

resource "aws_sns_topic_subscription" "postToNotionDB" {
  topic_arn = aws_sns_topic.dict_data_topic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.postToNotionDB.arn
}

resource "aws_sns_topic_subscription" "create_image" {
  topic_arn = aws_sns_topic.dict_data_topic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.create_image.arn
}
