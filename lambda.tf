resource "aws_lambda_function" "orchestrator_function" {
  function_name = var.orchestrator_function
  role          = aws_iam_role.lambda_roles["sns_publish"].arn
  s3_bucket     = aws_s3_bucket.lambdas.id
  s3_key        = "${var.orchestrator_function}_function.zip"
  architectures = ["arm64"]
  runtime       = "python3.9"
  handler       = "orchestrator.lambda_handler"
}

resource "aws_lambda_permission" "notionDB_function_permission" {
  action        = "lambda:InvokeFunction"
  function_name = var.orchestrator_function
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.vocab-app-back-end.execution_arn}/*/*/postword"
}

resource "aws_lambda_function" "postToNotionDB_function" {
  function_name = var.postToNotionDB_function
  role          = aws_iam_role.lambda_roles["only_sqs_receive"].arn
  s3_bucket     = aws_s3_bucket.lambdas.id
  s3_key        = "${var.postToNotionDB_function}_function.zip"
  architectures = ["arm64"]
  runtime       = "nodejs16.x"
  handler       = "index.handler"
  layers        = [aws_lambda_layer_version.node_layer.arn]
  environment {
    variables = {
      NOTION_API_TOKEN = var.NOTION_API_TOKEN
      WORD_DATABASE_ID = var.WORD_DATABASE_ID
    }
  }
}

resource "aws_lambda_layer_version" "node_layer" {
  layer_name               = var.node_layer_name
  compatible_architectures = ["arm64"]
  compatible_runtimes      = ["nodejs16.x"]
  description              = "Layer for node.js (${var.postToNotionDB_function} function)"
  s3_bucket                = aws_s3_bucket.lambdas.id
  s3_key                   = "lambda_${var.node_layer_name}.zip"
}

resource "aws_lambda_function" "create_image_function" {
  function_name = var.create_image_function
  role          = aws_iam_role.lambda_roles["s3_upload"].arn
  s3_bucket     = aws_s3_bucket.lambdas.id
  s3_key        = "${var.create_image_function}_function.zip"
  timeout       = 10
  architectures = ["x86_64"]
  runtime       = "python3.9"
  handler       = "generate_image.lambda_handler"
  layers        = ["arn:aws:lambda:eu-west-2:770693421928:layer:Klayers-p39-pillow:1"]
}
