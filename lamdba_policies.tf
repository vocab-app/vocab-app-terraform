resource "aws_iam_policy" "sns_publishing_policy" {
  name   = "sns-publish-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sns:Publish"
        ],
      "Effect": "Allow",
      "Resource": "${aws_sns_topic.dict_data_topic.arn}",
      "Sid": "1"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "sqs_receive_messages" {
  for_each = {
    (var.postToNotionDB_function) = aws_sqs_queue.postToNotionDB.arn,
    (var.create_image_function)   = aws_sqs_queue.create_image.arn
  }

  name   = "${each.key}-sqs-receive-messages"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sqs:ReceiveMessage",
        "sqs:DeleteMessage",
        "sqs:GetQueueAttributes"
        ],
      "Effect": "Allow",
      "Resource": "${each.value}",
      "Sid": "1"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "function_logging_policies" {
  for_each = toset([
    var.orchestrator_function,
    var.postToNotionDB_function,
    var.create_image_function
  ])

  name   = "${each.value}-logging-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
        ],
      "Effect": "Allow",
      "Resource": "arn:aws:logs:eu-west-2:753108572620:log-group:/aws/lambda/${each.value}:*",
      "Sid": "1"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "upload_to_s3" {
  name   = "upload-to-s3-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:putObject",
        "s3:putObjectAcl"
        ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${var.wallpaper_bucket_name}/*",
      "Sid": "1"
    }
  ]
}
EOF
}
