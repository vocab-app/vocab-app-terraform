variable "NOTION_API_TOKEN" {}
variable "WORD_DATABASE_ID" {}

variable "aws_region" {
  type        = string
  default     = "eu-west-2"
  description = "default AWS region"
}

variable "frontend_bucketname" {
  type        = string
  default     = "vocab-app-frontend"
  description = "Bucket name for the static website S3 bucket"
}

variable "lambda_bucket_name" {
  type        = string
  default     = "vocab-app-lambdas"
  description = "Bucket name for storing Lambdas"
}

variable "orchestrator_function" {
  type        = string
  default     = "orchestrator"
  description = "The name of the orchestrator function"
}

variable "postToNotionDB_function" {
  type        = string
  default     = "postToNotionDB"
  description = "The name of the postToNotionDB function"
}

variable "node_layer_name" {
  type        = string
  default     = "node_layer"
  description = "The name of the Node.JS lambda layer"
}

variable "lambda_roles_list" {
  type        = list(string)
  default     = ["sns_publish", "only_sqs_receive", "s3_upload"]
  description = "A list of lambda role names"
}

variable "api_name" {
  type        = string
  default     = "vocab-app-http-api"
  description = "The name of the Vocab App API"
}

variable "api_default_stage_name" {
  type        = string
  default     = "$default"
  description = "The default stage name for Vocab App API"
}

variable "wallpaper_bucket_name" {
  type        = string
  default     = "vocab-app-wallpapers"
  description = "Bucket name for storing wallpaper images"
}

variable "create_image_function" {
  type        = string
  default     = "create_image"
  description = "The name of the create_image function"
}
