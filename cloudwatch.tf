resource "aws_cloudwatch_log_group" "lambda_log_groups" {
  for_each = toset([
    var.orchestrator_function,
    var.postToNotionDB_function,
    var.create_image_function
  ])

  name              = "/aws/lambda/${each.value}"
  retention_in_days = 3
}
