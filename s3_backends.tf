resource "aws_s3_bucket" "lambdas" {
  bucket = var.lambda_bucket_name
}

resource "aws_s3_bucket" "wallpaper-s3" {
  bucket = var.wallpaper_bucket_name
}
