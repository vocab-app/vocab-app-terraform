resource "aws_sqs_queue" "postToNotionDB" {
  name = "postToNotionDB-queue"
}

resource "aws_sqs_queue" "create_image" {
  name = "create-image-queue"
}

resource "aws_sqs_queue_policy" "postToNotionDB" {
  queue_url = aws_sqs_queue.postToNotionDB.id
  policy    = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": [
        "sqs:SendMessage"
      ],
      "Resource": [
        "${aws_sqs_queue.postToNotionDB.arn}"
      ],
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${aws_sns_topic.dict_data_topic.arn}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_sqs_queue_policy" "create_image" {
  queue_url = aws_sqs_queue.create_image.id
  policy    = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": [
        "sqs:SendMessage"
      ],
      "Resource": [
        "${aws_sqs_queue.create_image.arn}"
      ],
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${aws_sns_topic.dict_data_topic.arn}"
        }
      }
    }
  ]
}
EOF
}

resource "aws_lambda_event_source_mapping" "postToNotionDB_sqs_mapping" {
  event_source_arn = aws_sqs_queue.postToNotionDB.arn
  function_name    = aws_lambda_function.postToNotionDB_function.arn
}

resource "aws_lambda_event_source_mapping" "create_image_sqs_mapping" {
  event_source_arn = aws_sqs_queue.create_image.arn
  function_name    = aws_lambda_function.create_image_function.arn
}
