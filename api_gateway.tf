resource "aws_apigatewayv2_api" "vocab-app-back-end" {
  name          = var.api_name
  description   = "API for Vocab App backend"
  protocol_type = "HTTP"
  cors_configuration {
    allow_origins = ["http://${aws_s3_bucket_website_configuration.vocab_app_frontend.website_endpoint}"]
    allow_methods = ["GET", "POST", "OPTIONS"]
    allow_headers = ["content-type"]
  }
}

resource "aws_apigatewayv2_deployment" "deploy_vocab_app" {
  api_id = aws_apigatewayv2_api.vocab-app-back-end.id
}

resource "aws_apigatewayv2_stage" "vocab_app_api_stage" {
  api_id      = aws_apigatewayv2_api.vocab-app-back-end.id
  name        = var.api_default_stage_name
  auto_deploy = true
}

resource "aws_apigatewayv2_integration" "orchestrator_integration" {
  api_id                 = aws_apigatewayv2_api.vocab-app-back-end.id
  description            = "Integrating the ${var.orchestrator_function} function"
  integration_type       = "AWS_PROXY"
  integration_method     = "POST"
  payload_format_version = "2.0"
  integration_uri        = aws_lambda_function.orchestrator_function.invoke_arn
}

resource "aws_apigatewayv2_route" "post_route" {
  api_id    = aws_apigatewayv2_api.vocab-app-back-end.id
  route_key = "POST /postword"
  target    = "integrations/${aws_apigatewayv2_integration.orchestrator_integration.id}"
}
